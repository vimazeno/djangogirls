## install dependencies

see https://stackoverflow.com/questions/19559247/requirements-txt-depending-on-python-version

### with python3

```
sudo apt-get install python3-dev libmysqlclient-dev mysql-server mysql-server # check $PATH to have mysql in
# pip install mysqlclient
pip install -r requirements/python3.txt
```

## with python2

```
sudo apt-get install python-dev libmysqlclient-dev mysql-server mysql-server # check $PATH to have mysql in
# pip install mysql-python
pip install -r requirements/python2.txt
```

## update dependencies

```
pip freeze > requirements.text
```

## generation

```
django-admin startproject mysite .
python manage.py startapp blog
python manage.py startapp cms
```

## init

```
cd static && npm install && cd ..
python manage.py makemigrations blog cms
python manage.py migrate blog
python manage.py migrate cms
python manage.py loaddata */fixtures/*.json
```

### add an admin

```
python manage.py createsuperuser --username admin --email admin@admin.com
```

### dump databases

```
./manage.py dumpdata --exclude auth.permission --exclude contenttypes --exclude admin.LogEntry --indent 2 > db.json
```

## further
* <strike>fixtures</strike>
* better [mptt](http://mptt.readthedocs.io/en/latest/) admin 
  * introspection to list object
  * drag an drop to reorder
* search http://haystacksearch.org/
* AD integration
  * fallback auth
  * ACL in tree
* JSON API for MySQL

-----------------------------------------------------------

## WSGI

an update from [https://community.apachefriends.org/f/viewtopic.php?t=42975](https://community.apachefriends.org/f/viewtopic.php?t=42975)

## To install Python in XAMPP:

I will use python system binary

## Enable xampp libtool for ubuntu

```
vi  /opt/lampp/build/libtool
```

replace first line

```
#! /bin/sh
```

by

```
#! /bin/bash
```

## install mod_wsgi in XAMPP:

```
cd /tmp
wget https://pypi.python.org/packages/aa/43/f851abaad631aee69206e29cebf9f8bf0ddb9c22dbd6e583f1f8f44e6d43/mod_wsgi-4.5.20.tar.gz
tar xvzf mod_wsgi-4.5.20.tar.gz -C /tmp
cd mod_wsgi-4.5.20/
./configure --prefix=/opt/lampp/ --with-apxs=/opt/lampp/bin/apxs --with-python=/usr/bin/python
make
sudo make install
```

## Let's add the proper PATH into Apache

I didn't need to add anything


## Apache /opt/lampp/etc/httpd.conf entries:

```
LoadModule wsgi_module modules/mod_wsgi.so
Include etc/extra/httpd-wsgi.conf
```


## Apache /opt/lampp/etc/extra/httpd-wsgi.conf entries:

```
WSGIPythonHome   /opt/lampp     # XAMPP uses an alternate Python: helps to shield from OS Python Pkg installation under /usr
WSGISocketPrefix /opt/lampp/var/run/wsgi     # non-standard location acceptable to Apache
Alias  /media/   /opt/lampp/pylons/mysite/media/      # make sure all Alias come before WSGIScriptAlias
WSGIScriptAlias  /wsgi-test/    /opt/lampp/var/wsgi/scripts-test/helloworld.wsgi
<Directory /opt/lampp/var/wsgi/scripts-test>
    WSGIApplicationGroup scripts-test
        Order allow,deny
        Require all granted # this one is needed since apache 2.4
</Directory>
```

## /opt/lampp/var/wsgi/scripts-test/helloworld.wsgi:

```
#!/usr/bin/python # I use python system bianry as i mentonned it
def application(environ, start_response):
  status = '200 OK'
  output = 'Hello World!'

  response_headers = [('Content-type', 'text/plain'),
                      ('Content-Length', str(len(output)))]
  start_response(status, response_headers)

  return [output]
```


## Restart Apache

```
/opt/lampp/lampp restart
```


browser to: [http://localhost/wsgi-test/helloworld.wsgi](http://localhost/wsgi-test/helloworld.wsgi) should display hello world!
