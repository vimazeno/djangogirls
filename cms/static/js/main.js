$(function() {
  $('#jstree').jstree({
    "core" : { // core options go here
      "multiple" : false, // no multiselection
      "themes" : {
        //"dots" : false // no connecting dots between dots
      }
    },
    "types" : {
      "#" : {
        "max_children" : 1,
        "max_depth" : 4,
        "valid_children" : ["root"]
      },
      'f-open' : {
        'icon' : 'fa fa-folder-open'
      },
      'f-closed' : {
        'icon' : 'fa fa-folder'
      },
      "file" : {
        "icon" : 'fa fa-file',
      }
    },
    "plugins" : ["types","state"] //,"dnd","contextmenu"]
  });
});

$('#jstree').jstree("deselect_all");

$("#jstree").on('open_node.jstree', function (event, data) {
    data.instance.set_type(data.node,'f-open');
});
$("#jstree").on('close_node.jstree', function (event, data) {
    data.instance.set_type(data.node,'f-closed');
});

$('#jstree').on('select_node.jstree', function (event, data) {
  if(data.event) {
    console.log(data.node);
  }
});
