from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^pages/$', views.page_list, name='page_list'),
]
