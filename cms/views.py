from django.shortcuts import render
from .models import Page

def page_list(request):
    return render(request, 'page/page_list.html', {'nodes':Page.objects.all()})
